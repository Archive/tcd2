/* A generic interface to cdrom drives.
   Tim P. Gerla <timg@means.net>
*/

#ifndef TCD_CDROM_H
#define TCD_CDROM_H

#include <glib.h>

typedef enum
{
    CD_NO_ERROR = 0,
    CD_IO_ERROR,
    CD_OUT_OF_RANGE,
    CD_INVALID_ARG,
    CD_INTERNAL_ERROR,
} CDError;

#define CD_FORMAT_MSF 0
#define CD_FORMAT_TRACK 1

typedef struct
{
    guint format;

    /* For format == CD_FORMAT_MSF */
    guint minute;
    guint second;
    guint frame;

    /* For format == CD_FORMAT_TRACK */
    guint track;
} CDTime;

typedef struct
{
    CDTime abs; /* current absolute time */
    CDTime rel; /* current time relative to track */

    int cur_track;

} CDInfo;

typedef struct
{
    gulong magic;		/* 0xdeadbeef */
    gchar *name;		/* name of the device */
    gchar *path;		/* pathname to device file */

    int fd;			/* file descriptor */

    gpointer pdata;		/* private data. don't touch. */
} CDStruct;

CDStruct *cd_new(gchar *path);
gint cd_init(CDStruct *cd);
gint cd_read(CDStruct *cd);
gint cd_stop(CDStruct *cd);
gint cd_get_info(CDStruct *cd, CDInfo *info);
gint cd_play(CDStruct *cd, CDTime *start, CDTime *end);

char *cd_strerror(int error);

gint _cd_open(CDStruct *cd);
gint _cd_close(CDStruct *cd);

#endif /* TCD_CDROM_H */
