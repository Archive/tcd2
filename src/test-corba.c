#include <libgnorba/gnorba.h>
#include "Cdrom.h"

GNOME_Cdrom_Controller control;

void Exception (CORBA_Environment * ev);

void Exception (CORBA_Environment * ev)
{
    switch (ev->_major)
    {
    case CORBA_SYSTEM_EXCEPTION:
        g_log ("GNOME CDAudio Client", G_LOG_LEVEL_DEBUG, "CORBA system exception %s.\n",
               CORBA_exception_id (ev));
        break;
    case CORBA_USER_EXCEPTION: 
        g_log ("GNOME CDAudio Client", G_LOG_LEVEL_DEBUG, "CORBA user exception: %s.\n",
               CORBA_exception_id (ev));
        break;     
    default:
        break;
    }
}

int main(int argc, char *argv[])
{
	CORBA_Environment ev;
	CORBA_ORB orb;
	GNOME_Cdrom_Time s, e;

	CORBA_exception_init(&ev);
	orb = gnome_CORBA_init("Cdrom Client", "0.0", &argc, argv, 0, &ev);

	control = CORBA_ORB_string_to_object(orb, argv[1], &ev);

	g_print("GNOME_Cdrom_Controller_get_disc_id returns: %s\n", GNOME_Cdrom_Controller_get_disc_id(control, "/dev/cdrom", &ev));
	
	s._d = GNOME_Cdrom_TRACK;
	s._u.track = 1;
	e._d = GNOME_Cdrom_TRACK;
	e._u.track = 4;

//	GNOME_Cdrom_Controller_play(control, "/dev/cdrom", &s, &e, &ev);

	CORBA_Object_release(control, &ev);
	CORBA_Object_release((CORBA_Object)orb, &ev);
   

}
