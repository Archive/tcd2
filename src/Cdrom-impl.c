#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <orb/orbit.h>
#include <libgnorba/gnorba.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/cdrom.h>

#include "Cdrom.h"
#include <errno.h>

/*** App-specific servant structures ***/









typedef struct
{
	POA_GNOME_Cdrom_Controller servant;
	PortableServer_POA poa;














}
impl_POA_GNOME_Cdrom_Controller;


typedef struct
{
	POA_GNOME_Cdrom_Client servant;
	PortableServer_POA poa;


}
impl_POA_GNOME_Cdrom_Client;





/*** Implementation stub prototypes ***/









static void
impl_GNOME_Cdrom_Controller__destroy (impl_POA_GNOME_Cdrom_Controller *
				      servant, CORBA_Environment * ev);



static CORBA_short
impl_GNOME_Cdrom_Controller_init (impl_POA_GNOME_Cdrom_Controller * servant,
				  CORBA_char * device,
				  CORBA_Environment * ev);

static void
impl_GNOME_Cdrom_Controller_close (impl_POA_GNOME_Cdrom_Controller * servant,
				   CORBA_char * device,
				   CORBA_Environment * ev);

static void
impl_GNOME_Cdrom_Controller_play (impl_POA_GNOME_Cdrom_Controller * servant,
				  CORBA_char * device,
				  GNOME_Cdrom_Time * start,
				  GNOME_Cdrom_Time * stop,
				  CORBA_Environment * ev);

static void
impl_GNOME_Cdrom_Controller_stop (impl_POA_GNOME_Cdrom_Controller * servant,
				  CORBA_char * device,
				  CORBA_Environment * ev);

static void
impl_GNOME_Cdrom_Controller_pause (impl_POA_GNOME_Cdrom_Controller * servant,
				   CORBA_char * device,
				   CORBA_Environment * ev);

static void
impl_GNOME_Cdrom_Controller_eject (impl_POA_GNOME_Cdrom_Controller * servant,
				   CORBA_char * device,
				   CORBA_Environment * ev);

static GNOME_Cdrom_State
impl_GNOME_Cdrom_Controller_get_state (impl_POA_GNOME_Cdrom_Controller *
				       servant, CORBA_char * device,
				       CORBA_Environment * ev);

static CORBA_short
impl_GNOME_Cdrom_Controller_get_num_tracks (impl_POA_GNOME_Cdrom_Controller *
					    servant, CORBA_char * device,
					    CORBA_Environment * ev);

static GNOME_Cdrom_Time
impl_GNOME_Cdrom_Controller_get_position (impl_POA_GNOME_Cdrom_Controller *
					  servant, GNOME_Cdrom_Format format,
					  CORBA_Environment * ev);

static CORBA_char
*impl_GNOME_Cdrom_Controller_get_disc_id (impl_POA_GNOME_Cdrom_Controller *
					  servant, CORBA_char * device,
					  CORBA_Environment * ev);

static GNOME_Cdrom_DiscInfo
*impl_GNOME_Cdrom_Controller_get_disc_titles
(impl_POA_GNOME_Cdrom_Controller * servant, CORBA_char * device,
 CORBA_Environment * ev);


static void impl_GNOME_Cdrom_Client__destroy (impl_POA_GNOME_Cdrom_Client *
					      servant,
					      CORBA_Environment * ev);
static void impl_GNOME_Cdrom_Client_media_changed (impl_POA_GNOME_Cdrom_Client
						   * servant,
						   CORBA_char * device,
						   CORBA_Environment * ev);

static void
impl_GNOME_Cdrom_Client_update_titles (impl_POA_GNOME_Cdrom_Client * servant,
				       CORBA_char * device,
				       CORBA_Environment * ev);





/*** epv structures ***/









static PortableServer_ServantBase__epv impl_GNOME_Cdrom_Controller_base_epv = {
	NULL,				/* _private data */
	NULL,				/* finalize routine */
	NULL,				/* default_POA routine */
};
static POA_GNOME_Cdrom_Controller__epv impl_GNOME_Cdrom_Controller_epv = {
	NULL,				/* _private */



	(gpointer) & impl_GNOME_Cdrom_Controller_init,

	(gpointer) & impl_GNOME_Cdrom_Controller_close,

	(gpointer) & impl_GNOME_Cdrom_Controller_play,

	(gpointer) & impl_GNOME_Cdrom_Controller_stop,

	(gpointer) & impl_GNOME_Cdrom_Controller_pause,

	(gpointer) & impl_GNOME_Cdrom_Controller_eject,

	(gpointer) & impl_GNOME_Cdrom_Controller_get_state,

	(gpointer) & impl_GNOME_Cdrom_Controller_get_num_tracks,

	(gpointer) & impl_GNOME_Cdrom_Controller_get_position,

	(gpointer) & impl_GNOME_Cdrom_Controller_get_disc_id,

	(gpointer) & impl_GNOME_Cdrom_Controller_get_disc_titles,

};
static PortableServer_ServantBase__epv impl_GNOME_Cdrom_Client_base_epv = {
	NULL,				/* _private data */
	NULL,				/* finalize routine */
	NULL,				/* default_POA routine */
};
static POA_GNOME_Cdrom_Client__epv impl_GNOME_Cdrom_Client_epv = {
	NULL,				/* _private */
	(gpointer) & impl_GNOME_Cdrom_Client_media_changed,

	(gpointer) & impl_GNOME_Cdrom_Client_update_titles,

};



/*** vepv structures ***/









static POA_GNOME_Cdrom_Controller__vepv impl_GNOME_Cdrom_Controller_vepv = {
	&impl_GNOME_Cdrom_Controller_base_epv,
	&impl_GNOME_Cdrom_Controller_epv,
};
static POA_GNOME_Cdrom_Client__vepv impl_GNOME_Cdrom_Client_vepv = {
	&impl_GNOME_Cdrom_Client_base_epv,
	&impl_GNOME_Cdrom_Client_epv,
};



/*** Stub implementations ***/

GNOME_Cdrom_Controller
impl_GNOME_Cdrom_Controller__create (PortableServer_POA poa,
				     CORBA_Environment * ev)
{
	GNOME_Cdrom_Controller retval;
	impl_POA_GNOME_Cdrom_Controller *newservant;
	PortableServer_ObjectId *objid;
	
	newservant = g_new0 (impl_POA_GNOME_Cdrom_Controller, 1);
	newservant->servant.vepv = &impl_GNOME_Cdrom_Controller_vepv;
	newservant->poa = poa;
	POA_GNOME_Cdrom_Controller__init ((PortableServer_Servant) newservant, ev);
	objid = PortableServer_POA_activate_object (poa, newservant, ev);
	CORBA_free (objid);
	retval = PortableServer_POA_servant_to_reference (poa, newservant, ev);

	return retval;
}

void
impl_GNOME_Cdrom_Controller__destroy (impl_POA_GNOME_Cdrom_Controller *
				      servant, CORBA_Environment * ev)
{
	PortableServer_ObjectId *objid;

	objid = PortableServer_POA_servant_to_id (servant->poa, servant, ev);
	PortableServer_POA_deactivate_object (servant->poa, objid, ev);
	CORBA_free (objid);

	POA_GNOME_Cdrom_Controller__fini ((PortableServer_Servant) servant, ev);
	g_free (servant);
}


static CORBA_short
impl_GNOME_Cdrom_Controller_init (impl_POA_GNOME_Cdrom_Controller * servant,
				  CORBA_char * device, CORBA_Environment * ev)
{
	return TRUE;
}


static void
impl_GNOME_Cdrom_Controller_close (impl_POA_GNOME_Cdrom_Controller * servant,
				   CORBA_char * device,
				   CORBA_Environment * ev)
{
}

gint _cd_open(char *device)
{
	gint fd;

	if((fd = open(device, O_RDONLY|O_NONBLOCK)) == -1)
	{
		g_print("Debug: cd_open: open(%s, ...) failed. reason: %s\n", device, strerror(errno));
		return -1;
	}
	return fd;
}

void _cd_close(gint fd)
{
	close(fd);
}


static void
impl_GNOME_Cdrom_Controller_play (impl_POA_GNOME_Cdrom_Controller * servant,
				  CORBA_char * device,
				  GNOME_Cdrom_Time * start,
				  GNOME_Cdrom_Time * stop,
				  CORBA_Environment * ev)
{
	gint error;
	gint fd;
	struct cdrom_tocentry toc;                    
	struct cdrom_tochdr tochdr;
	int first_t, last_t;
	struct cdrom_msf msf;

	if((fd = _cd_open(device)) == -1)
		return -1; /* exception */
    
	if(ioctl(fd, CDROMREADTOCHDR, &tochdr))
	{
		_cd_close(fd);
		return -1; /* exception */
	}

	first_t = tochdr.cdth_trk0;
	last_t = tochdr.cdth_trk1;

	/* get the start time */
	if(start->_d == GNOME_Cdrom_TRACK)
	{
		toc.cdte_track = start->_u.track;
		toc.cdte_format = CDROM_MSF;
		if(ioctl(fd, CDROMREADTOCENTRY, &toc))
			return -1; /* FIXME exception here */  
		msf.cdmsf_min0 = toc.cdte_addr.msf.minute;
		msf.cdmsf_sec0 = toc.cdte_addr.msf.second;
		msf.cdmsf_frame0 = toc.cdte_addr.msf.frame;
	} else {
		msf.cdmsf_min0 = start->_u.msf.minute;
		msf.cdmsf_sec0 = start->_u.msf.second;
		msf.cdmsf_frame0 = start->_u.msf.frame;
	}

	/* get the ending time */
	if(stop->_d == GNOME_Cdrom_TRACK)
	{
		if(++stop->_u.track >= last_t)
			stop->_u.track = CDROM_LEADOUT;
		toc.cdte_track = stop->_u.track;   
		toc.cdte_format = CDROM_MSF;   
		if(ioctl(fd, CDROMREADTOCENTRY, &toc))
			return -1; /* FIXME exception here */
		msf.cdmsf_min1 = toc.cdte_addr.msf.minute;
		msf.cdmsf_sec1 = toc.cdte_addr.msf.second;
		msf.cdmsf_frame0 = toc.cdte_addr.msf.frame;  
	} else {
		msf.cdmsf_min0 = start->_u.msf.minute;
		msf.cdmsf_sec0 = start->_u.msf.second;
		msf.cdmsf_frame0 = start->_u.msf.frame;
	}

	/* play it */

	error = ioctl(fd, CDROMPLAYMSF, &msf);
	_cd_close(fd);

	return error;
}

static void
impl_GNOME_Cdrom_Controller_stop (impl_POA_GNOME_Cdrom_Controller * servant,
				  CORBA_char * device, CORBA_Environment * ev)
{
	gint fd, error;

	if((fd = _cd_open(device)) == -1)
		return -1; /* exception */

	error = ioctl(fd, CDROMSTOP);
	_cd_close(fd);

	/* exception or something */
	return error;
}


static void
impl_GNOME_Cdrom_Controller_pause (impl_POA_GNOME_Cdrom_Controller * servant,
				   CORBA_char * device,
				   CORBA_Environment * ev)
{
}


static void
impl_GNOME_Cdrom_Controller_eject (impl_POA_GNOME_Cdrom_Controller * servant,
				   CORBA_char * device,
				   CORBA_Environment * ev)
{
	g_warning("Eject is not yet implemented\n");
}


static GNOME_Cdrom_State
impl_GNOME_Cdrom_Controller_get_state (impl_POA_GNOME_Cdrom_Controller *
				       servant, CORBA_char * device,
				       CORBA_Environment * ev)
{
	GNOME_Cdrom_State retval;
	g_print("get_state is not yet implemented.\n");
	return retval;
}


static CORBA_short
impl_GNOME_Cdrom_Controller_get_num_tracks (impl_POA_GNOME_Cdrom_Controller *
					    servant, CORBA_char * device,
					    CORBA_Environment * ev)
{
	CORBA_short retval;

	return retval;
}


static GNOME_Cdrom_Time
impl_GNOME_Cdrom_Controller_get_position (impl_POA_GNOME_Cdrom_Controller *
					  servant, GNOME_Cdrom_Format format,
					  CORBA_Environment * ev)
{
	GNOME_Cdrom_Time retval;

	return retval;
}


static CORBA_char *
impl_GNOME_Cdrom_Controller_get_disc_id (impl_POA_GNOME_Cdrom_Controller *
					 servant, CORBA_char * device,
					 CORBA_Environment * ev)
{
	struct {
		guint minute, second, frame;
	} time[128];

	gint fd;
	char str[12];
	gint32 error, i, n=0, t=0;
	struct cdrom_tocentry toc;
	struct cdrom_tochdr tochdr;
	int first_t, last_t;  
	
	if((fd = _cd_open(device)) == -1)
		return ""; /* exception */
	
	if(ioctl(fd, CDROMREADTOCHDR, &tochdr))
	{
		_cd_close(fd);
		return "";
	}
        
	first_t = tochdr.cdth_trk0;
	last_t = tochdr.cdth_trk1;
	
	for(i = 0; i <= last_t; i++)
	{
		toc.cdte_track = (i==last_t)?CDROM_LEADOUT:i+1;
		toc.cdte_format = CDROM_MSF;
		if(ioctl(fd, CDROMREADTOCENTRY, &toc))
		{
			_cd_close(fd);
			return "";
		}
		time[i].minute = toc.cdte_addr.msf.minute;
		time[i].second = toc.cdte_addr.msf.second;
	}
	
	for(i = 0; i < last_t; i++)
		n += cddb_sum((time[i].minute * 60) + time[i].second);
	
	t = ((time[last_t].minute * 60) + time[last_t].second) -
		((time[0].minute * 60) + time[0].second);
	
	_cd_close(fd);
        
	g_snprintf(str, 12, "%08lx", ((n % 0xff) << 24 | t << 8 | last_t));
	return CORBA_string_dup(str);
}

static int cddb_sum(int n)
{
	char buf[12], *p; 
	int ret = 0;
	
	sprintf(buf, "%u", n);
	for (p = buf; *p != '\0'; p++)
		ret += (*p - '0');
	
	return (ret);
}


static GNOME_Cdrom_DiscInfo *
impl_GNOME_Cdrom_Controller_get_disc_titles (impl_POA_GNOME_Cdrom_Controller *
					     servant, CORBA_char * device,
					     CORBA_Environment * ev)
{
	GNOME_Cdrom_DiscInfo *retval;

	return retval;
}



static GNOME_Cdrom_Client
impl_GNOME_Cdrom_Client__create (PortableServer_POA poa,
				 CORBA_Environment * ev)
{
	GNOME_Cdrom_Client retval;
	impl_POA_GNOME_Cdrom_Client *newservant;
	PortableServer_ObjectId *objid;

	newservant = g_new0 (impl_POA_GNOME_Cdrom_Client, 1);
	newservant->servant.vepv = &impl_GNOME_Cdrom_Client_vepv;
	newservant->poa = poa;
	POA_GNOME_Cdrom_Client__init ((PortableServer_Servant) newservant, ev);
	objid = PortableServer_POA_activate_object (poa, newservant, ev);
	CORBA_free (objid);
	retval = PortableServer_POA_servant_to_reference (poa, newservant, ev);

	return retval;
}

static void
impl_GNOME_Cdrom_Client__destroy (impl_POA_GNOME_Cdrom_Client * servant,
				  CORBA_Environment * ev)
{
	PortableServer_ObjectId *objid;

	objid = PortableServer_POA_servant_to_id (servant->poa, servant, ev);
	PortableServer_POA_deactivate_object (servant->poa, objid, ev);
	CORBA_free (objid);

	POA_GNOME_Cdrom_Client__fini ((PortableServer_Servant) servant, ev);
	g_free (servant);
}

static void
impl_GNOME_Cdrom_Client_media_changed (impl_POA_GNOME_Cdrom_Client * servant,
				       CORBA_char * device,
				       CORBA_Environment * ev)
{
	g_print("hi, we're in media_changed.");
}


static void
impl_GNOME_Cdrom_Client_update_titles (impl_POA_GNOME_Cdrom_Client * servant,
				       CORBA_char * device,
				       CORBA_Environment * ev)
{
	g_print("Hi, we're in update_titles.");
}

void Exception (CORBA_Environment * ev);

void Exception (CORBA_Environment * ev)
{
	switch (ev->_major)
	{
	case CORBA_SYSTEM_EXCEPTION:
		g_log ("Gnome CD Audio Server", G_LOG_LEVEL_DEBUG, "CORBA system exception %s.\n",
		       CORBA_exception_id (ev));                                              
		exit (1);
	case CORBA_USER_EXCEPTION:
		g_log ("Gnome CD Audio Server", G_LOG_LEVEL_DEBUG, "CORBA user exception: %s.\n",
		       CORBA_exception_id (ev));                                             
		exit (1);
	default:
		break;
	}
}

/* begin server stuff */
GNOME_Cdrom_Controller *controller;
PortableServer_POA poa;
CORBA_Environment ev;
CORBA_ORB orb;

int main(int argc, char *argv[])
{
	char *ior;
	PortableServer_POAManager poa_manager;

	CORBA_exception_init(&ev);
	orb = gnome_CORBA_init("cdrom-controller", "0.0", &argc, argv, GNORBA_INIT_SERVER_FUNC, &ev);
	poa = (PortableServer_POA)
		CORBA_ORB_resolve_initial_references(orb, "RootPOA", &ev);
	Exception(&ev);

	poa_manager = PortableServer_POA__get_the_POAManager (poa, &ev);
	Exception(&ev);

	PortableServer_POAManager_activate (poa_manager, &ev);
	Exception(&ev);
  
	controller = impl_GNOME_Cdrom_Controller__create(poa, &ev);
	Exception(&ev);

	ior = CORBA_ORB_object_to_string(orb, controller, &ev);
	g_print("IOR: %s\n", ior);
	CORBA_free(ior);

	goad_server_register(NULL, controller, "cdrom-controller", "object", &ev);

	CORBA_ORB_run(orb, &ev);
	Exception(&ev);

	goad_server_unregister(NULL, "cdrom-controller", "object", &ev);

	return 0;
}
