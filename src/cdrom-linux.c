/* Copyright (C) 1999 Tim P. Gerla <timg@means.net>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   Tim P. Gerla
   RR 1, Box 40
   Climax, MN  56523
   timg@means.net
*/

#include <glib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <linux/cdrom.h>

#include "cdrom.h"

char *cd_error[] = {
    "No error",
    "IO error",
    "Parameter out of range",
    "Invalid argument",
    "Internal error",
};

CDStruct *cd_new(gchar *path)
{
    CDStruct *cd;

    cd = g_new0(CDStruct, 1);
    cd->path = path;
    cd->magic = 0xdeadbeef;

    return cd;
}

gint _cd_open(CDStruct *cd)
{
    g_return_if_fail(cd->magic == 0xdeadbeef);

    if((cd->fd = open(cd->path, O_RDONLY|O_NONBLOCK)) == -1)
    {
	g_print("Debug: cd_open: open(%s, ...) failed. reason: %s\n", cd->path, strerror(errno));
	return CD_IO_ERROR;
    }
    return CD_NO_ERROR;
}

gint _cd_close(CDStruct *cd)
{
    g_return_val_if_fail(cd->magic == 0xdeadbeef, CD_INVALID_ARG);

    close(cd->fd);
    cd->fd = -1;
    
    return CD_NO_ERROR;
}

int cd_stop(CDStruct *cd)
{
    int error;

    g_return_val_if_fail(cd->magic == 0xdeadbeef, CD_INVALID_ARG);
    
    if((error = _cd_open(cd)) != 0)
	return error;
    
    if(ioctl(cd->fd, CDROMSTOP))
    {
	_cd_close(cd);
	return CD_IO_ERROR;
    }

    if((error = _cd_close(cd)) != 0)
	return error;

    return CD_NO_ERROR;
}

gint cd_get_info(CDStruct *cd, CDInfo *info)
{
    struct cdrom_subchnl sub;
    int error;
    CDTitles titles;
    char *id, *fname;

    if((error = _cd_open(cd)) != 0)
	return error;

    sub.cdsc_format = CDROM_MSF;
    if(ioctl(cd->fd, CDROMSUBCHNL, &sub))
    {
	_cd_close(cd);
	return CD_IO_ERROR;
    }

    info->rel.minute = sub.cdsc_reladdr.msf.minute;
    info->rel.second = sub.cdsc_reladdr.msf.second;
    info->rel.frame  = sub.cdsc_reladdr.msf.frame;

    info->abs.minute = sub.cdsc_reladdr.msf.minute;
    info->abs.second = sub.cdsc_reladdr.msf.second;
    info->abs.frame  = sub.cdsc_reladdr.msf.frame;

    info->cur_track = sub.cdsc_trk;
    
    if((error = _cd_close(cd)) != 0)
	return error;

    return CD_NO_ERROR;
}

gint cd_play(CDStruct *cd, CDTime *start, CDTime *end)
{
    gint error;
    CDTime *s, *e;
    struct cdrom_tocentry toc;
    struct cdrom_tochdr tochdr;
    int first_t, last_t;
    struct cdrom_msf msf;

    g_return_val_if_fail(cd->magic == 0xdeadbeef, CD_INVALID_ARG);

    if((error = _cd_open(cd)) != 0)
	return error;

    if(ioctl(cd->fd, CDROMREADTOCHDR, &tochdr))
    {
	_cd_close(cd);
	return CD_IO_ERROR;
    }

    first_t = tochdr.cdth_trk0;
    last_t = tochdr.cdth_trk1;

    /* get the start time */
    if(start->format == CD_FORMAT_TRACK)
    {
	toc.cdte_track = start->track;
	toc.cdte_format = CDROM_MSF;
	if(ioctl(cd->fd, CDROMREADTOCENTRY, &toc))
	    return CD_IO_ERROR;
	start->minute = toc.cdte_addr.msf.minute;
	start->second = toc.cdte_addr.msf.second;
	start->frame = toc.cdte_addr.msf.frame;
    }

    /* get the ending time */
    if(end->format == CD_FORMAT_TRACK)
    {
	if(++end->track >= last_t)
	    end->track = CDROM_LEADOUT;
	toc.cdte_track = end->track;
	toc.cdte_format = CDROM_MSF;
	if(ioctl(cd->fd, CDROMREADTOCENTRY, &toc))
	    return CD_IO_ERROR;
	end->minute = toc.cdte_addr.msf.minute;
	end->second = toc.cdte_addr.msf.second;
	end->frame = toc.cdte_addr.msf.frame;
    }

    /* play it */
    msf.cdmsf_min0 = start->minute;
    msf.cdmsf_sec0 = start->second;
    msf.cdmsf_frame0 = start->frame;
    
    msf.cdmsf_min1 = end->minute;
    msf.cdmsf_sec1 = end->second;
    msf.cdmsf_frame1 = end->frame;
    
    if(ioctl(cd->fd, CDROMPLAYMSF, &msf))
    {
	_cd_close(cd);
	return CD_IO_ERROR;
    }

    if((error = _cd_close(cd)) != 0)
	return error;

    return CD_NO_ERROR;
}

gint cd_dump_info(CDStruct *cd)
{
    gint error, i;
    struct cdrom_tocentry toc;
    struct cdrom_tochdr tochdr;
    int first_t, last_t;
    char *id, *fname;
    CDTitles titles;

    g_return_val_if_fail(cd->magic == 0xdeadbeef, CD_INVALID_ARG);

    id = cd_cddb_id(cd);
    g_print("CDDB Id: %s\n", id);
    fname = g_strdup_printf("/home/timg/.cddbslave/%s", id);
    cd_read_cddb(&titles, fname);
    printf("Title: %s\n", titles.title);

    g_return_val_if_fail(cd->magic == 0xdeadbeef, CD_INVALID_ARG);

    if((error = _cd_open(cd)) != 0)
    {
	_cd_close(cd);
	return error;
    }

    if(ioctl(cd->fd, CDROMREADTOCHDR, &tochdr))
    {
	_cd_close(cd);
	return CD_IO_ERROR;
    }

    first_t = tochdr.cdth_trk0;
    last_t = tochdr.cdth_trk1;

    /* get the start time */
    for(i = first_t; i <= last_t; i++)
    {
	toc.cdte_track = i;
	toc.cdte_format = CDROM_MSF;
	if(ioctl(cd->fd, CDROMREADTOCENTRY, &toc))
	{
	    _cd_close(cd);
	    return CD_IO_ERROR;
	}

	printf("Track %2d: %2dm %2ds %2df\t%s\n", i,
	      toc.cdte_addr.msf.minute,
	      toc.cdte_addr.msf.second,
	      toc.cdte_addr.msf.frame, titles.track[i].title);
    }

    if((error = _cd_close(cd)) != 0)
	return error;

    return CD_NO_ERROR;
}

char *cd_cddb_id(CDStruct *cd)
{
    CDTime time[128];
    gint32 error, i, n=0, t=0;
    struct cdrom_tocentry toc;
    struct cdrom_tochdr tochdr;
    int first_t, last_t;

    g_return_val_if_fail(cd->magic == 0xdeadbeef, NULL);

    if((error = _cd_open(cd)) != 0)
    {
	_cd_close(cd);
	return NULL;
    }

    if(ioctl(cd->fd, CDROMREADTOCHDR, &tochdr))
    {
	_cd_close(cd);
	return NULL;
    }

    first_t = tochdr.cdth_trk0;
    last_t = tochdr.cdth_trk1;

    for(i = 0; i <= last_t; i++)
    {
	toc.cdte_track = (i==last_t)?CDROM_LEADOUT:i+1;
	toc.cdte_format = CDROM_MSF;
	if(ioctl(cd->fd, CDROMREADTOCENTRY, &toc))
	{
	    _cd_close(cd);
	    return NULL;
	}
	time[i].minute = toc.cdte_addr.msf.minute;
	time[i].second = toc.cdte_addr.msf.second;
    }

    for(i = 0; i < last_t; i++)
	n += cddb_sum((time[i].minute * 60) + time[i].second);

    t = ((time[last_t].minute * 60) + time[last_t].second) -
	((time[0].minute * 60) + time[0].second);
 
    if((error = _cd_close(cd)) != 0)
	return NULL;
        
    return g_strdup_printf("%08lx", ((n % 0xff) << 24 | t << 8 | last_t));
}

int cddb_sum(int n)
{
    char buf[12], *p; 
    int ret = 0;
    
    sprintf(buf, "%u", n);
    for (p = buf; *p != '\0'; p++)
	ret += (*p - '0');
    
    return (ret);
}

char *cd_strerror(int error)
{
    return cd_error[error];
}

