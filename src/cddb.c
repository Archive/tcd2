#include <stdio.h>
#include <glib.h>
#include <unistd.h>

#include "cdrom.h"


static int num_digits( int num)
{
    int count = 1;
    
    while ((num/=10) != 0)
	count++;
    return(count);
}

int cd_read_cddb(CDTitles *cd, gchar* filename)
{
    FILE* fp;
    char string[256];
    int trk;
    unsigned long rev;
    
    /* zero out the extended data sections ... */
    cd->rev = 0;
    cd->ext[0] = '\0';
    for(trk = 0; trk < 128; trk++)
	cd->track[trk].ext[0] = '\0';
    
    fp = fopen(filename, "r");
    
    if(fp == NULL)
	return -2;

    while(fgets(string, 255, fp)!=NULL)
    {
	string[strlen(string)-1] = 0;
	if(strncmp(string, "# Revision:", 11) == 0)
	{
	    if (sscanf(string, "# Revision: %lu", &rev) == 1)
		cd->rev = rev;
	}
	    
	/* If it's a comment, ignore. */
	if(string[0] == '#')
	    continue;
	    
	/* If it's the disc title, print it */
	if(strncmp(string, "DTITLE", 6) == 0)
	{
	    strncpy(cd->title, string+7, DISC_INFO_LEN);
	    continue;
	}
	if(strncmp(string, "TTITLE", 6) == 0)
	{
	    if(sscanf(string, "TTITLE%d=", &trk) == 1)
		strncpy(cd->track[trk+1].title, 
			string + 7 + num_digits(trk),
			TRK_NAME_LEN);
	    else
		cd->track[trk+1].title[0] = 0;
	}
	/* extra data for the disc */
	if (strncmp(string, "EXTD", 4) == 0)
	{
	    char *tmp = &string[5];
	    int i, j;
	    /* convert embedded new lines */
	    for (i = j = 0; tmp[i] != '\0'; i++, j++)
		if (tmp[i]=='\\' && tmp[i+1]=='n') {
		    tmp[j] = '\n';
		    i++;
		} else if (tmp[i]=='\\' && tmp[i+1]=='t') {
		    tmp[j] = '\t';
		    i++;
		} else if (tmp[i]=='\\' && tmp[i+1]=='\\') {
		    tmp[j] = '\\';
		    i++;
		} else
		    tmp[j] = tmp[i];
	    tmp[j] = '\0';
	    strncat(cd->ext, tmp, EXT_DATA_LEN);
	}
	/* extra data for a track */
	if (strncmp(string, "EXTT", 4) == 0)
	{
	    if (sscanf(string, "EXTT%d=", &trk) == 1) {
		char *tmp = &string[5 + num_digits(trk)];
		int i, j;
				/* convert embedded new lines */
		for (i = j = 0; tmp[i] != '\0'; i++, j++)
		{
		    if (tmp[i]=='\\' && tmp[i+1]=='n') {
			tmp[j] = '\n';
			i++;
		    } else if(tmp[i]=='\\'&&tmp[i+1]=='t'){
			tmp[j] = '\t';
			i++;
		    }else if(tmp[i]=='\\'&&tmp[i+1]=='\\'){
			tmp[j] = '\\';
			i++;
		    } else
			tmp[j] = tmp[i];
		}
		tmp[j] = '\0';
		strncat(cd->track[trk+1].ext, tmp, EXT_DATA_LEN);
	    }
	}
	/* Otherwise ignore it */
    }
    fclose(fp);
    return 0;
}
