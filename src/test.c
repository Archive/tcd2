#include "cdrom.h"
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <linux/cdrom.h>

int main(int argc, char *argv[])
{
    char *id;
    char *fname;
    CDStruct *cd;
    int i, error;
    CDTitles titles;

    cd = cd_new("/dev/cdrom");
    cd_dump_info(cd);

    for(i = 1; i < argc; i++)
    {
	char *span = argv[i];
	CDTime start, end;

	if(!strncmp(span, "stop", 4))
	    cd_stop(cd);
	else if(span)
	{
	    char *span2 = strchr(span, '-');
	    if(strrchr(span, '-') != span2)
	    {
		printf("Error parsing play range.\n");
		return -1;
	    }
	    
	    if(span2 != NULL)
	    {
		*span2 = 0;
		span2++;
	    }
	    
	    /* parse the first span */
	    if(strchr(span, '[')) /* we have a time */
	    {
		if(3 != sscanf(span, "[%d:%d.%d]", &start.minute, &start.second, &start.frame))
		{
		    if(2 != sscanf(span, "[%d:%d]", &start.minute, &start.second))
		    {
			printf("Error parsing play range: %s\n", span);
			return -1;
		    }
		    start.frame = 0;
		}
		start.format = CD_FORMAT_MSF;
	    }
	    else
	    {
		start.format = CD_FORMAT_TRACK;
		start.track = atoi(span);
	    }
	    
	    /* parse teh second span */
	    if(strchr(span2, '[')) /* we have a time */
	    {
		if(3 != sscanf(span2, "[%d:%d.%d]", &end.minute, &end.second, &end.frame))
		{
		    if(2 != sscanf(span2, "[%d:%d]", &end.minute, &end.second))
		    {
			printf("Error parsing play range: %s\n", span2);
			return -1;
		    }
		    end.frame = 0;
		}
		end.format = CD_FORMAT_MSF;
	    }
	    else
	    {
		end.format = CD_FORMAT_TRACK;
		end.track = atoi(span2);
	    }

	    error = cd_play(cd, &start, &end);
	    if(error)
		printf("%s: %s\n", cd_strerror(error), strerror(errno));
	}
	else
	{
	    printf("Invalid span.\n", span);
	}
    }
}
